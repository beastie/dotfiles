;; -*- lexical-binding: t -*-

;; makes emacs loadtime faster by reducing the garbage collector
(setq gc-cons-threshold (* 50 1000 1000))


;;; --- UI --- ;;;
;; hides tool bar, menu bar and scroll bar
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)

;; enable line wrap
(global-visual-line-mode t)

;; turns yes or no questions to y or n
(defalias 'yes-or-no-p 'y-or-n-p)

;; Sets font to my preferred font
(add-to-list 'default-frame-alist
             '(font . "monospace-10"))


;; icons for emacs
(use-package all-the-icons
  :ensure t)

;; go syntax highlighting
(use-package go-mode)

;; sets up doom mode line bar (make sure to install all-the-icons pakage and then run M-x all-the-icons-install-fonts to get all the icons properly) 
(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1))

;; always kill the current buffer
(defun kill-curr-buffer ()
  (interactive)
  (kill-buffer (current-buffer)))
(global-set-key (kbd "C-x k") 'kill-curr-buffer)

;; sets the theme
(use-package doom-themes
      :ensure t
      :config
      (load-theme 'doom-gruvbox t)
      (doom-themes-org-config)
      (doom-themes-visual-bell-config))

;; colored brackets and other delimiters useful for programming and debugging
(use-package rainbow-delimiters)

;; show hexcodes as their color eg. show #fff as a white highlight
(use-package rainbow-mode
  :ensure t
  :init (add-hook 'prog-mode-hook 'rainbow-mode))

(add-hook 'prog-mode-hook #'rainbow-delimiters-mode) ;; automatically start the program in all programming modes
(setq inhibit-startup-message t) ;; Turns off default dashboard

;;; --- DASHBOARD --- ;;;

;; Makes the dashboard buffer the default for when you use the emacs daemon
(setq initial-buffer-choice (lambda () (get-buffer "*dashboard*")))

;; sets up the superior dashboard package
(use-package dashboard
    :ensure t
    :config
    (dashboard-setup-startup-hook)
    (setq dashboard-startup-banner "~/.emacs.d/imgs/beastie_avatar_circle.png")
    (setq dashboard-banner-logo-title "Every solution becomes a problem ~ puffy")
    (setq dashboard-set-heading-icons t)
    (setq dashboard-set-file-icons t))

;;; --- MAGIT --- ;;;

(use-package magit
  :ensure t
  :config
  (setq magit-push-always-verify nil)
  :bind
  ("C-c g" . magit-status)) ;; Magit keybinding

;; Makes it so magit works with my dotfiles repo(git bare repo)
(defun ~/magit-process-environment (env)
  (let ((default (file-name-as-directory (expand-file-name default-directory)))
        (home (expand-file-name "~/")))
    (when (string= default home)
      (let ((gitdir (expand-file-name "~/.dotfiles/"))) ;; bare repo directory
        (push (format "GIT_WORK_TREE=/home/anya" home) env) ;; bare repo's work tree
        (push (format "GIT_DIR=/home/anya/.dotfiles" gitdir) env)))) ;; absolute path to the git bare repository
  env)

(advice-add 'magit-process-environment
            :filter-return #'~/magit-process-environment)

;;; --- LINE NUMBERS --- ;;;

;; line numbers in all buffer
(global-display-line-numbers-mode)

;; disable line numbers for some modes
(dolist (mode '(org-mode-hook ;; org document
                term-mode-hook ;; terminal
		vterm-mode-hook ;; another terminal
                shell-mode-hook ;; shell
                eshell-mode-hook ;; another shell
                treemacs-mode-hook ;; filetree
		completion-list-mode-hook ;; auto completion buffer
		org-agenda-mode-hook ;; agenda viewer
		calendar-mode-hook ;; emacs calendar
		tetris-mode-hook)) ;; tetris
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;;; --- ORG-MODE --- ;;; 

;; org mode improvements
(setq org-ellipsis " ▾") ;; cool icon for when lists/trees in org mode are folded
(setq org-src-fontify-natively t)
(setq org-src-tab-acts-natively t)
(setq org-confirm-babel-evaluate nil)
(setq org-export-with-smart-quotes t)
(setq org-src-window-setup 'current-window)
(setq org-agenda-files '("~/doc/org/agenda.org"))
(global-set-key (kbd "C-c a") #'org-agenda)
(setq org-startup-indented t) 

;; fancy org-mode bullets
(add-hook 'org-mode-hook 'org-indent-mode)(use-package org-bullets
  :ensure t
  :custom
  (org-bullets-bullet-list '("●" "●" "●" "●" "●" "●")) ;; custom bullet list because I don't like the default one C:
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

;;; --- MISC --- ;;;
(setq ring-bell-function 'ignore)

;; UTF-8 encoding things
(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)

;; sets melpa as the package archive
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

;; tells emacs to NOT make backups and not to autosave stuff
(setq make-backup-files nil)
(setq auto-save-default nil)

(use-package evil) ;; vim emulation
(evil-mode 1)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("96871555d64815a906796e1e594a91245c2327c2bf57ec87ddf9c1668ef6069c" default))
 '(org-agenda-files nil)
 '(package-selected-packages
   '(rainbow-mode all-the-icons rainbow-delimiters doom-modeline magit org-bullets evil use-package doom-themes dashboard go-mode)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
